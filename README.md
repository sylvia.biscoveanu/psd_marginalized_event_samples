## Posterior sample release for GWTC1 events including marginalization over PSD uncertainty

All results files can be read in using json directly:
```
result = json.load(open('GW150914_marginalized_result.json','r'))
chirp_mass_posterior = result['posterior']['content']['chirp_mass']
```
or using the bilby package:
```
result = bilby.gw.result.CBCResult.from_json('GW150914_marginalized_result.json')
chirp_mass_posterior = result.posterior['chirp_mass']
```
Other parameters can be accessed in a similar way. Using bilby to load the result file will create a bilby result object, which has many useful features besides just accessing the posterior samples. These include plotting capabilites, skymap generation, and accessing the priors used for the analysis, for example. 
For more information about bilby result objects and files, see the [documentation](https://lscsoft.docs.ligo.org/bilby/bilby-output.html) and the [source code](https://git.ligo.org/lscsoft/bilby/-/blob/master/bilby/gw/result.py).

Each event folder contains the PSD-marginalized and median PSD posterior samples in the corresponding `result.json` file, as well as the median PSD and all the individual PSD posterior samples for each detector used for that particular event.